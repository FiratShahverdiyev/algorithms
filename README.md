<h1>Solutions of Hackerrank DataStructure questions</h1>
<h2>Tree</h2>
<table>
<thead>
<tr>
<th>#</th>
<th>Title</th>
<th>Solutions</th>
<th>Difficulty</th>
<th>Tag</th>
</tr>
</thead>
<tbody>

<tr>
<tr>
<td>1</td>
<td><a href="https://www.hackerrank.com/challenges/is-binary-search-tree/copy-from/194901624" rel="nofollow">Is This a Binary Search Tree? </a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/tree/IsThisBinarySearchTree.java">Solution</a></td>
<td>Medium</td>
<td>Tree</td>
</tr>
</tr>

<tr>
<tr>
<td>2</td>
<td><a href="https://www.hackerrank.com/challenges/tree-preorder-traversal/problem" rel="nofollow">Tree: Preorder Traversal</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/tree/PreOrderTraversal.java">Solution</a></td>
<td>Easy</td>
<td>Tree</td>
</tr>
</tr>

<tr>
<tr>
<td>3</td>
<td><a href="https://www.hackerrank.com/challenges/tree-postorder-traversal/problem" rel="nofollow">Tree: Postorder Traversal</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/tree/PostOrderTraversal.java">Solution</a></td>
<td>Easy</td>
<td>Tree</td>
</tr>
</tr>

<tr>
<tr>
<td>4</td>
<td><a href="https://www.hackerrank.com/challenges/tree-inorder-traversal/problem" rel="nofollow">Tree: Inorder Traversal</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/tree/InOrderTraversal.java">Solution</a></td>
<td>Easy</td>
<td>Tree</td>
</tr>
</tr>

<tr>
<tr>
<td>5</td>
<td><a href="https://www.hackerrank.com/challenges/tree-height-of-a-binary-tree/problem" rel="nofollow">Tree: Height of a Binary Tree</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/tree/HeightOfBinaryTree.java">Solution</a></td>
<td>Easy</td>
<td>Tree</td>
</tr>
</tr>

<tr>
<tr>
<td>6</td>
<td><a href="https://www.hackerrank.com/challenges/binary-search-tree-insertion/problem" rel="nofollow">Binary Search Tree : Insertion</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/tree/BinarySearchTreeInsertion.java">Solution</a></td>
<td>Easy</td>
<td>Tree</td>
</tr>
</tr>

<tr>
<tr>
<td>7</td>
<td><a href="https://www.hackerrank.com/challenges/swap-nodes-algo/problem" rel="nofollow">Swap Nodes [Algo]</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/tree/SwapNode.java">Solution</a></td>
<td>Medium</td>
<td>Tree</td>
</tr>
</tr>

<tr>
<tr>
<td>8</td>
<td><a href="https://www.hackerrank.com/challenges/tree-huffman-decoding/problem" rel="nofollow">Tree: Huffman Decoding</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/tree/HuffmanDecoding.java">Solution</a></td>
<td>Medium</td>
<td>Tree</td>
</tr>
</tr>

<tr>
<tr>
<td>9</td>
<td><a href="https://www.hackerrank.com/challenges/binary-search-tree-lowest-common-ancestor/problem" rel="nofollow">Binary Search Tree : Lowest Common Ancestor</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/tree/LowestCommonAncestor.java">Solution</a></td>
<td>Easy</td>
<td>Tree</td>
</tr>
</tr>

</table>

<h2>Array</h2>
<table>
<thead>
<tr>
<th>#</th>
<th>Title</th>
<th>Solutions</th>
<th>Difficulty</th>
<th>Tag</th>
</tr>
</thead>
<tbody>

<tr>
<tr>
<td>1</td>
<td><a href="https://www.hackerrank.com/challenges/arrays-ds/problem" rel="nofollow">Arrays - DS</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/array/ArrayDS.java">Solution</a></td>
<td>Easy</td>
<td>Array</td>
</tr>
</tr>

<tr>
<tr>
<td>2</td>
<td><a href="https://www.hackerrank.com/challenges/2d-array/problem" rel="nofollow">2D Array - DS</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/array/ArrayDS_2D.java">Solution</a></td>
<td>Easy</td>
<td>Array</td>
</tr>
</tr>

<tr>
<tr>
<td>3</td>
<td><a href="https://www.hackerrank.com/challenges/dynamic-array/problem" rel="nofollow">Dynamic Array</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/array/DynamicArray.java">Solution</a></td>
<td>Easy</td>
<td>Array</td>
</tr>
</tr>

<tr>
<tr>
<td>4</td>
<td><a href="https://www.hackerrank.com/challenges/array-left-rotation/problem" rel="nofollow">Left Rotation</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/array/LeftRotation.java">Solution</a></td>
<td>Easy</td>
<td>Array</td>
</tr>
</tr>

<tr>
<tr>
<td>5</td>
<td><a href="https://www.hackerrank.com/challenges/sparse-arrays/problem" rel="nofollow">Sparse Arrays</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/array/SparseArrays.java">Solution</a></td>
<td>Medium</td>
<td>Array</td>
</tr>
</tr>

<tr>
<tr>
<td>6</td>
<td><a href="https://www.hackerrank.com/challenges/crush/problem" rel="nofollow">Array Manipulation</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/array/ArrayManipulation.java">Solution</a></td>
<td>Hard</td>
<td>Array</td>
</tr>
</tr>

</table>

<h2>Stack</h2>
<table>
<thead>
<tr>
<th>#</th>
<th>Title</th>
<th>Solutions</th>
<th>Difficulty</th>
<th>Tag</th>
</tr>
</thead>
<tbody>

<tr>
<tr>
<td>1</td>
<td><a href="https://www.hackerrank.com/challenges/balanced-brackets/problem" rel="nofollow">Balanced Brackets</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/stack/BalancedBrackets.java">Solution</a></td>
<td>Medium</td>
<td>Stack</td>
</tr>
</tr>

<tr>
<tr>
<td>2</td>
<td><a href="https://www.hackerrank.com/challenges/equal-stacks/problem" rel="nofollow">Equal Stacks</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/stack/EqualStacks.java">Solution</a></td>
<td>Easy</td>
<td>Stack</td>
</tr>
</tr>

<tr>
<tr>
<td>3</td>
<td><a href="https://www.hackerrank.com/challenges/maximum-element/problem" rel="nofollow">Maximum Element</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/stack/MaximumElement.java">Solution</a></td>
<td>Easy</td>
<td>Stack</td>
</tr>
</tr>

<tr>
<tr>
<td>4</td>
<td><a href="https://www.hackerrank.com/challenges/simple-text-editor/problem" rel="nofollow">Simple Text Editor</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/stack/SimpleTextEditor.java">Solution</a></td>
<td>Medium</td>
<td>Stack</td>
</tr>
</tr>

<tr>
<tr>
<td>5</td>
<td><a href="https://www.hackerrank.com/challenges/waiter/problem" rel="nofollow">Waiter</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/stack/Waiter.java">Solution</a></td>
<td>Medium</td>
<td>Stack</td>
</tr>
</tr>

</table>

<h2>Linked List</h2>
<table>
<thead>
<tr>
<th>#</th>
<th>Title</th>
<th>Solutions</th>
<th>Difficulty</th>
<th>Tag</th>
</tr>
</thead>
<tbody>

<tr>
<tr>
<td>1</td>
<td><a href="https://www.hackerrank.com/challenges/print-the-elements-of-a-linked-list/problem" rel="nofollow">Print the Elements of a Linked List</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/PrintLinkedList.java">Solution</a></td>
<td>Easy</td>
<td>Linked List</td>
</tr>
</tr>

<tr>
<tr>
<td>2</td>
<td><a href="https://www.hackerrank.com/challenges/insert-a-node-at-the-tail-of-a-linked-list/problem" rel="nofollow">Insert a Node at the Tail of a Linked List</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/InsertNodeAtTail.java">Solution</a></td>
<td>Easy</td>
<td>Linked List</td>
</tr>
</tr>

<tr>
<tr>
<td>3</td>
<td><a href="https://www.hackerrank.com/challenges/insert-a-node-at-the-head-of-a-linked-list/problem" rel="nofollow">Insert a Node at the Head of a Linked List</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/InsertNodeAtHead.java">Solution</a></td>
<td>Easy</td>
<td>Linked List</td>
</tr>
</tr>

<tr>
<tr>
<td>4</td>
<td><a href="https://www.hackerrank.com/challenges/insert-a-node-at-a-specific-position-in-a-linked-list/problem" rel="nofollow">Insert a Node at the Specific Position of a Linked List</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/InsertNodeSpecific.java">Solution</a></td>
<td>Easy</td>
<td>Linked List</td>
</tr>
</tr>

<tr>
<tr>
<td>5</td>
<td><a href="https://www.hackerrank.com/challenges/delete-a-node-from-a-linked-list/problem" rel="nofollow">Delete a Node</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/DeleteNode.java">Solution</a></td>
<td>Easy</td>
<td>Linked List</td>
</tr>
</tr>

<tr>
<tr>
<td>6</td>
<td><a href="https://www.hackerrank.com/challenges/print-the-elements-of-a-linked-list-in-reverse/problem" rel="nofollow">Print in Reverse</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/PrintReverse.java">Solution</a></td>
<td>Easy</td>
<td>Linked List</td>
</tr>
</tr>

<tr>
<tr>
<td>7</td>
<td><a href="https://www.hackerrank.com/challenges/reverse-a-linked-list/problem" rel="nofollow">Reverse a Linked List</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/ReverseLinkedList.java">Solution</a></td>
<td>Easy</td>
<td>Linked List</td>
</tr>
</tr>

<tr>
<tr>
<td>8</td>
<td><a href="https://www.hackerrank.com/challenges/compare-two-linked-lists/problem" rel="nofollow">Compare two Linked Lists</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/CompareTwoLinkedList.java">Solution</a></td>
<td>Easy</td>
<td>Linked List</td>
</tr>
</tr>

<tr>
<tr>
<td>9</td>
<td><a href="https://www.hackerrank.com/challenges/merge-two-sorted-linked-lists/problem" rel="nofollow">Merge two sorted Linked Lists</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/MergeTwoLinkedList.java">Solution</a></td>
<td>Easy</td>
<td>Linked List</td>
</tr>
</tr>

<tr>
<tr>
<td>10</td>
<td><a href="https://www.hackerrank.com/challenges/get-the-value-of-the-node-at-a-specific-position-from-the-tail/problem" rel="nofollow">Get Node Value</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/GetNodeValue.java">Solution</a></td>
<td>Easy</td>
<td>Linked List</td>
</tr>
</tr>

<tr>
<tr>
<td>11</td>
<td><a href="https://www.hackerrank.com/challenges/delete-duplicate-value-nodes-from-a-sorted-linked-list/problem" rel="nofollow">Delete duplicate-value nodes from a sorted linked list</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/DeleteDuplicateValue.java">Solution</a></td>
<td>Easy</td>
<td>Linked List</td>
</tr>
</tr>

<tr>
<tr>
<td>12</td>
<td><a href="https://www.hackerrank.com/challenges/detect-whether-a-linked-list-contains-a-cycle/problem" rel="nofollow">Cycle Detection</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/CycleDetection.java">Solution</a></td>
<td>Medium</td>
<td>Linked List</td>
</tr>
</tr>

<tr>
<tr>
<td>13</td>
<td><a href="https://www.hackerrank.com/challenges/find-the-merge-point-of-two-joined-linked-lists/problem" rel="nofollow">Find Merge Point of Two Lists</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/FindMergePoint.java">Solution</a></td>
<td>Easy</td>
<td>Linked List</td>
</tr>
</tr>

<tr>
<tr>
<td>14</td>
<td><a href="https://www.hackerrank.com/challenges/insert-a-node-into-a-sorted-doubly-linked-list/problem" rel="nofollow">Inserting a Node Into a Sorted Doubly Linked List</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/InsertingNodeSortedDoubly.java">Solution</a></td>
<td>Easy</td>
<td>Linked List</td>
</tr>
</tr>

<tr>
<tr>
<td>15</td>
<td><a href="https://www.hackerrank.com/challenges/reverse-a-doubly-linked-list/problem" rel="nofollow">Reverse a doubly linked list</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/linkedlist/ReverseDoublyLinkedList.java">Solution</a></td>
<td>Easy</td>
<td>Linked List</td>
</tr>
</tr>

</table>

<h2>Queue</h2>
<table>
<thead>
<tr>
<th>#</th>
<th>Title</th>
<th>Solutions</th>
<th>Difficulty</th>
<th>Tag</th>
</tr>
</thead>
<tbody>

<tr>
<tr>
<td>1</td>
<td><a href="https://www.hackerrank.com/challenges/queue-using-two-stacks/problem" rel="nofollow">Queue using Two Stacks</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/queue/QueueTwoStack.java">Solution</a></td>
<td>Medium</td>
<td>Queue</td>
</tr>
</tr>

</table>

<h2>Heap</h2>
<table>
<thead>
<tr>
<th>#</th>
<th>Title</th>
<th>Solutions</th>
<th>Difficulty</th>
<th>Tag</th>
</tr>
</thead>
<tbody>

<tr>
<tr>
<td>1</td>
<td><a href="https://www.hackerrank.com/challenges/jesse-and-cookies/problem" rel="nofollow">Jesse and Cookies</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/queue/JessieAndCookies.java">Solution</a></td>
<td>Easy</td>
<td>Queue</td>
</tr>
</tr>

</table>


<h1>Solutions of Leetcode problems</h1>
<h2>Best Questions</h2>
<table>
<thead>
<tr>
<th>#</th>
<th>Title</th>
<th>Solutions</th>
<th>Difficulty</th>
<th>Tag</th>
</tr>
</thead>
<tbody>

<tr>
<tr>
<td>1</td>
<td><a href="https://leetcode.com/problems/container-with-most-water/" rel="nofollow">Container With Most Water</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/leetcode/bestquestion/ContainerWithWater.java">Solution</a></td>
<td>Medium</td>
<td>Array,Two Pointer</td>
</tr>
</tr>

<tr>
<tr>
<td>2</td>
<td><a href="https://leetcode.com/problems/remove-nth-node-from-end-of-list/" rel="nofollow">Remove Nth Node From End of List</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/leetcode/bestquestion/RemoveNthNodeLinked.java">Solution</a></td>
<td>Medium</td>
<td>LinkedList,Two Pointer</td>
</tr>
</tr>

<tr>
<tr>
<td>3</td>
<td><a href="https://leetcode.com/problems/swap-nodes-in-pairs/" rel="nofollow">Swap Nodes in Pairs</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/leetcode/bestquestion/SwapNodes.java">Solution</a></td>
<td>Medium</td>
<td>LinkedList</td>
</tr>
</tr>

</table>

<h2>Concurrency</h2>
<table>
<thead>
<tr>
<th>#</th>
<th>Title</th>
<th>Solutions</th>
<th>Difficulty</th>
<th>Tag</th>
</tr>
</thead>
<tbody>

<tr>
<tr>
<td>1</td>
<td><a href="https://leetcode.com/problems/print-in-order/" rel="nofollow">Print in Order</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/leetcode/concurrency/PrintInOrder.java">Solution</a></td>
<td>Easy</td>
<td>Concurrency</td>
</tr>
</tr>

<tr>
<tr>
<td>2</td>
<td><a href="https://leetcode.com/problems/print-foobar-alternately/" rel="nofollow">Print FooBar Alternately</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/leetcode/concurrency/PrintFooBar.java">Solution</a></td>
<td>Medium</td>
<td>Concurrency</td>
</tr>
</tr>

<tr>
<tr>
<td>3</td>
<td><a href="https://leetcode.com/problems/print-zero-even-odd/" rel="nofollow">Print Zero Even Odd</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/leetcode/concurrency/ZeroEvenOdd.java">Solution</a></td>
<td>Medium</td>
<td>Concurrency</td>
</tr>
</tr>

<tr>
<tr>
<td>4</td>
<td><a href="https://leetcode.com/problems/the-dining-philosophers/" rel="nofollow">The Dining Philosophers</a></td>
<td><a href="https://gitlab.com/FiratShahverdiyev/algorithms/-/blob/master/DataStructures/src/com/company/leetcode/concurrency/DiningPhilosophers.java">Solution-1</a></td>
<td>Medium</td>
<td>Concurrency</td>
</tr>
</tr>

</table>
