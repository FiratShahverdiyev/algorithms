package com.company.array;

public class ArrayManipulation {
    static long arrayManipulation(int n, int[][] queries) {
        long[] arr = new long[n];
        int index1, index2;
        long addNumber;
        long max = 0L;
        long sum = 0;

        for (int i = 0; i < queries.length; i++) {
            index1 = queries[i][0] - 1;
            index2 = queries[i][1] - 1;
            addNumber = queries[i][2];
            arr[index1] += addNumber;
            if (index2 < n - 1)
                arr[index2 + 1] -= addNumber;
        }
        for (int i = 0; i < n; i++) {
            sum += arr[i];
            if (sum > max) {
                max = sum;
            }
        }
        return max;
    }
}
