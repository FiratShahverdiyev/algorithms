package com.company.array;

import java.util.ArrayList;
import java.util.List;

public class DynamicArray {
    public static List<Integer> dynamicArray(int n, List<List<Integer>> queries) {
        List<Integer> result = new ArrayList<>();
        List<Integer> list ;
        List<List<Integer>> allLists = new ArrayList<>(n);
        for (int i=0;i<n;i++){
            allLists.add(new ArrayList<>());
        }
        int lastAnswer = 0;

        for (List<Integer> query : queries) {
            list = query;
            int x = list.get(1);
            int y = list.get(2);
            int index = (x ^ lastAnswer) % n;
            if (list.get(0) == 1) {
                allLists.get(index).add(y);
            } else {
                lastAnswer = allLists.get(index).get(y % allLists.get(index).size());
                result.add(lastAnswer);
            }
        }
        return result;
    }
}
