package com.company.array;

import java.util.ArrayList;
import java.util.List;

public class LeftRotation {
    public static List<Integer> rotateLeft(int d, List<Integer> arr) {
        List<Integer> copy_arr = new ArrayList<>(arr);
        for (int i = 0; i < arr.size(); i++) {
            arr.set(i, copy_arr.get((i + d) % arr.size()));
        }
        return arr;
    }
}
