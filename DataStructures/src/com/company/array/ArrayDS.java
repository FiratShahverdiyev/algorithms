package com.company.array;

public class ArrayDS {
    static int[] reverseArray(int[] a) {
        int[] b = new int[a.length];
        for (int i = a.length - 1; i >= 0; i--) {
            b[a.length - i - 1] = a[i];
        }
        return b;
    }
}
