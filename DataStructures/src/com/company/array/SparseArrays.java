package com.company.array;

import java.util.HashMap;

public class SparseArrays {
    static int[] matchingStrings(String[] strings, String[] queries) {
        HashMap<String, Integer> frequency = new HashMap<>();
        for (int i = 0; i < strings.length; i++) {
            if (frequency.containsKey(strings[i])) {
                frequency.put(strings[i], frequency.get(strings[i]) + 1);
            } else {
                frequency.put(strings[i], 1);
            }
        }
        int[] arr = new int[queries.length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = frequency.getOrDefault(queries[i], 0);
        }
        return arr;
    }
}
