package com.company.leetcode.concurrency;

import java.util.concurrent.CountDownLatch;

public class PrintFooBar {
    private int n;
    private CountDownLatch first;
    private CountDownLatch second;

    public PrintFooBar(int n) {
        first = new CountDownLatch(1);
        second = new CountDownLatch(0);
        this.n = n;
    }

    public void foo(Runnable printFoo) throws InterruptedException {
        for (int i = 0; i < n; i++) {
            // printFoo.run() outputs "foo". Do not change or remove this line.
            second.await();
            second = new CountDownLatch(1);
            printFoo.run();
            first.countDown();
        }
    }

    public void bar(Runnable printBar) throws InterruptedException {
        for (int i = 0; i < n; i++) {
            first.await();
            first = new CountDownLatch(1);
            // printBar.run() outputs "bar". Do not change or remove this line.
            printBar.run();
            second.countDown();
        }
    }
}
