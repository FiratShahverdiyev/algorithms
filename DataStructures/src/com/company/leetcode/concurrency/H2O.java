package com.company.leetcode.concurrency;

import java.util.LinkedList;
import java.util.Queue;

public class H2O {
    private Queue<Runnable> o, h;

    public H2O() {
        o = new LinkedList<>();
        h = new LinkedList<>();
    }

    public void hydrogen(Runnable releaseHydrogen) throws InterruptedException {
        h.add(releaseHydrogen);
        checkAndRun();
        // releaseHydrogen.run() outputs "H". Do not change or remove this line.
//        releaseHydrogen.run();
    }

    public void oxygen(Runnable releaseOxygen) throws InterruptedException {
        o.add(releaseOxygen);
        checkAndRun();
        // releaseOxygen.run() outputs "O". Do not change or remove this line.
//        releaseOxygen.run();
    }

    public synchronized void checkAndRun() {
        if (o.size() >= 1 && h.size() >= 2) {
            o.remove().run();
            h.remove().run();
            h.remove().run();
        }
    }
}
