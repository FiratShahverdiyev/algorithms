package com.company.leetcode.bestquestion;

public class ContainerWithWater {
    public int maxArea(int[] height) {
        int p1 = 0;
        int p2 = height.length - 1;
        int maxArea = Integer.MIN_VALUE;
        while (p1 < p2) {
            if (height[p1] <= height[p2]) {
                maxArea = Math.max(maxArea, height[p1] * (p2 - p1));
                p1++;
            } else {
                maxArea = Math.max(maxArea, height[p2] * (p2 - p1));
                p2--;
            }
        }
        return maxArea;
    }
}
