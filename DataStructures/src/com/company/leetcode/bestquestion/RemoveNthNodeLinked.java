package com.company.leetcode.bestquestion;

import java.util.HashMap;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 * int val;
 * ListNode next;
 * ListNode() {}
 * ListNode(int val) { this.val = val; }
 * ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
public class RemoveNthNodeLinked {
    public ListNode removeNthFromEnd(ListNode head, int n) {
        HashMap<Integer, ListNode> nodeByIndex = new HashMap<>();
        ListNode iter = head;
        int nodes = 0;
        int index = 0;
        while (iter != null) {
            nodes++;
            nodeByIndex.put(index, iter);
            index++;
            iter = iter.next;
        }
        iter = nodeByIndex.get(index - n);
        if (iter.next != null) {
            if (nodeByIndex.get(index - n - 1) != null) {
                iter = nodeByIndex.get(index - n - 1);
                iter.next = iter.next.next;
            } else {
                head = head.next;
            }
        } else {
            if (nodeByIndex.get(index - n - 1) != null) {
                iter = nodeByIndex.get(index - n - 1);
                iter.next = iter.next.next;
            } else {
                head = null;
            }
        }
        return head;
    }
}
