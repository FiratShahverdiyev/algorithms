package com.company.leetcode.bestquestion;

import java.util.ArrayList;
import java.util.List;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 * int val;
 * ListNode next;
 * ListNode() {}
 * ListNode(int val) { this.val = val; }
 * ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
public class SwapNodes {
    public ListNode swapPairs(ListNode head) {
        if (head == null)
            return head;
        List<ListNode> nodes = new ArrayList<>();
        ListNode iter = head;
        while (iter != null) {
            nodes.add(iter);
            iter = iter.next;
        }
        for (int i = 0; i < nodes.size() - 1; i += 2) {
            ListNode node1 = nodes.get(i + 1);
            node1.next = null;
            ListNode temp = nodes.get(i);
            temp.next = null;
            nodes.set(i, node1);
            nodes.set(i + 1, temp);
        }
        head = nodes.get(0);
        iter = head;
        for (int i = 1; i < nodes.size(); i++) {
            System.out.println(iter.val);
            iter.next = nodes.get(i);
            iter = iter.next;
        }
        return head;
    }
}
