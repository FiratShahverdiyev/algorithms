package com.company.queue;

import java.util.Scanner;
import java.util.Stack;

public class QueueTwoStack {
    public static void main(String[] args) {
        int opr;
        Scanner sc = new Scanner(System.in);
        Stack<Integer> stack = new Stack<>();
        Stack<Integer> reverseStack = new Stack<>();
        int q = sc.nextInt();
        for (int i = 0; i < q; i++) {
            opr = sc.nextInt();
            if (opr == 1) {
                int x = sc.nextInt();
                stack.push(x);
            } else {
                if (reverseStack.isEmpty())
                    while (!stack.isEmpty()) {
                        reverseStack.push(stack.pop());
                    }
                if (opr == 2)
                    reverseStack.pop();
                else
                    System.out.println(reverseStack.peek());
            }
        }
    }
}
