package com.company.stack;

import java.util.Scanner;
import java.util.Stack;

public class SimpleTextEditor {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        StringBuffer str = new StringBuffer();
        int n = sc.nextInt();
        sc.nextLine();
        Stack<String> stack = new Stack<>();
        String s = "";
        for (int i = 0; i < n; i++) {
            s = sc.nextLine();
            if (s.charAt(0) == '1') {
                str.append(s.substring(2));
                stack.push(str.toString());
            } else if (s.charAt(0) == '2') {
                int k = Integer.parseInt(s.substring(2));
                int leng = str.length();
                str.delete(leng - k, leng);
                stack.push(str.toString());
            } else if (s.charAt(0) == '3') {
                int k = Integer.parseInt(s.substring(2));
                System.out.println(str.charAt(k - 1));
            } else {

                stack.pop();
                if (!stack.isEmpty()) {
                    s = stack.peek();
                } else {
                    s = "";
                }
                str = null;
                str = new StringBuffer(s);
            }
        }
    }
}
