package com.company.stack;

import java.util.Scanner;
import java.util.Stack;

public class MaximumElement {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] queries = new int[n][2];
        for (int i = 0; i < n; i++) {
            queries[i][0] = sc.nextInt();
            if (queries[i][0] == 1) {
                queries[i][1] = sc.nextInt();
            }
        }
        int max = 0;
        Stack<StackNode> stack = new Stack<>();
        for (int i = 0; i < n; i++) {
            if (queries[i][0] == 1) {
                int data = queries[i][1];
                if (data > max) {
                    max = data;
                }
                stack.push(new StackNode(data, max));

            } else if (queries[i][0] == 2) {
                stack.pop();
                if (!stack.isEmpty())
                    max = stack.peek().max;
                else
                    max = 0;
            } else {
                System.out.println(stack.peek().max);
            }
        }
    }

    public static class StackNode {
        int val;
        int max;

        public StackNode(int val, int max) {
            this.val = val;
            this.max = max;
        }
    }
}
