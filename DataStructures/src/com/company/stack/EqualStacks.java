package com.company.stack;

import java.util.Stack;

public class EqualStacks {
    static class StackNode {
        int sum;
        int val;

        public StackNode(int sum, int val) {
            this.sum = sum;
            this.val = val;
        }
    }

    static int equalStacks(int[] h1, int[] h2, int[] h3) {
        Stack<StackNode> stack1 = new Stack<>();
        Stack<StackNode> stack2 = new Stack<>();
        Stack<StackNode> stack3 = new Stack<>();
        int sum1 = 0, sum2 = 0, sum3 = 0;
        int max = Math.max(h1.length, h2.length);
        max = Math.max(max, h3.length);
        for (int i = max; i >= 0; i--) {
            if (i < h1.length) {
                sum1 += h1[i];
                stack1.push(new StackNode(sum1, h1[i]));
            }
            if (i < h2.length) {
                sum2 += h2[i];
                stack2.push(new StackNode(sum2, h2[i]));
            }
            if (i < h3.length) {
                sum3 += h3[i];
                stack3.push(new StackNode(sum3, h3[i]));
            }
        }
        while (!stack1.isEmpty() && !stack2.isEmpty() && !stack3.isEmpty()) {
            sum1 = stack1.peek().sum;
            sum2 = stack2.peek().sum;
            sum3 = stack3.peek().sum;
            if (sum1 == sum2 && sum2 == sum3) {
                return sum1;
            } else {
                if ((sum1 > sum2 && sum1 >= sum3) || (sum1 >= sum2 && sum1 > sum3)) {
                    stack1.pop();
                } else if (sum2 > sum1 && sum2 >= sum3) {
                    stack2.pop();
                } else {
                    stack3.pop();
                }
            }
        }
        return 0;
    }
}
