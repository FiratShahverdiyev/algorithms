package com.company.stack;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Waiter {
    static List<Integer> findPrimes() {
        List<Integer> primes = new ArrayList<>();
        boolean flag = true;
        for (int number = 2; primes.size() <= 1200; number++) {
            for (int divisor = 2; divisor <= Math.sqrt(number); divisor++) {
                if (number % divisor == 0) {
                    flag = false;
                    break;
                }
            }
            if (flag)
                primes.add(number);
            flag = true;
        }
        return primes;
    }

    static int[] waiter(int[] number, int q) {
        Stack<Integer> initialStack = new Stack<>();
        Stack<Integer> nonDivisible = new Stack<>();
        List<Integer> primes = findPrimes();
        for (int i = 0; i < number.length; i++) {
            initialStack.push(number[i]);
        }
        List<Stack<Integer>> stacks = new ArrayList<>();
        int prime;
        int j = 1;
        while (j <= q) {
            prime = primes.get(j - 1);
            j++;
            Stack<Integer> divisible = new Stack<>();
            while (!initialStack.isEmpty()) {
                if (initialStack.peek() % prime == 0) {
                    divisible.push(initialStack.pop());
                } else {
                    nonDivisible.push(initialStack.pop());
                }
            }
            stacks.add(divisible);
            initialStack = nonDivisible;
            if (j <= q)
                nonDivisible = new Stack<>();
        }
        j = 0;
        for (int i = 0; i < stacks.size(); i++) {
            Stack<Integer> stack = stacks.get(i);
            while (!stack.isEmpty()) {
                number[j] = stack.pop();
                j++;
            }
        }
        while (!nonDivisible.isEmpty()) {
            number[j] = nonDivisible.pop();
            j++;
        }
        return number;
    }
}
