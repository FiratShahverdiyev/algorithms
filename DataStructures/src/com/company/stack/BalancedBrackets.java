package com.company.stack;

import java.util.Stack;

public class BalancedBrackets {
    static String isBalanced(String s) {
        Stack<Character> stack = new Stack<>();
        char bracket = 'x';
        for (int i = 0; i < s.length(); i++) {
            bracket = s.charAt(i);
            if (bracket == '(' || bracket == '[' || bracket == '{') {
                stack.push(bracket);
            } else if (stack.isEmpty()) {
                return "NO";
            } else if (bracket == ')' && stack.peek() == '(') {
                stack.pop();
            } else if (bracket == ']' && stack.peek() == '[') {
                stack.pop();
            } else if (bracket == '}' && stack.peek() == '{') {
                stack.pop();
            } else {
                return "NO";
            }
        }
        if (stack.isEmpty()) {
            return "YES";
        } else {
            return "NO";
        }

    }
}
