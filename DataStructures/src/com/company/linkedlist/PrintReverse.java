package com.company.linkedlist;

import java.util.Stack;

public class PrintReverse {
    static void reversePrint(SinglyLinkedListNode head) {
        /*
         SinglyLinkedListNode {
         int data;
         SinglyLinkedListNode next;
     }
     */
        Stack<Integer> stack = new Stack<>();
        SinglyLinkedListNode iter = head;
        while (iter != null) {
            stack.add(iter.data);
            iter = iter.next;
        }
        while (!stack.isEmpty())
            System.out.println(stack.pop());
    }
}
