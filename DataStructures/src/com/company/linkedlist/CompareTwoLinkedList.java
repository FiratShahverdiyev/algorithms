package com.company.linkedlist;

public class CompareTwoLinkedList {
    /*
         SinglyLinkedListNode {
         int data;
         SinglyLinkedListNode next;
     }
     */
    static boolean compareLists(SinglyLinkedListNode head1, SinglyLinkedListNode head2) {
        int x1, x2;
        SinglyLinkedListNode iter1 = head1;
        SinglyLinkedListNode iter2 = head2;
        while (iter1 != null && iter2 != null) {
            x1 = iter1.data;
            x2 = iter2.data;
            if (x1 != x2) {
                return false;
            }
            iter1 = iter1.next;
            iter2 = iter2.next;
        }
        if (iter1 == null && iter2 == null)
            return true;

        return false;
    }
}
