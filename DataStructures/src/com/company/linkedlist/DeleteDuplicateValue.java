package com.company.linkedlist;

public class DeleteDuplicateValue {
    /*
     * For your reference:
     *
     * SinglyLinkedListNode {
     *     int data;
     *     SinglyLinkedListNode next;
     * }
     *
     */
    static SinglyLinkedListNode removeDuplicates(SinglyLinkedListNode head) {
        int duplicate = 0;
        SinglyLinkedListNode iter1 = head;
        SinglyLinkedListNode iter2 = head;
        while (iter1 != null) {
            duplicate = iter1.data;
            while (iter2.next != null && iter2.next.data == duplicate) { // 1 1 1 2 2
                iter2 = iter2.next;
            }
            iter1.next = iter2.next;
            iter1 = iter1.next;
            iter2 = iter1;
        }
        return head;
    }
}
