package com.company.linkedlist;

import java.util.Stack;

public class ReverseLinkedList {
    static SinglyLinkedListNode reverse(SinglyLinkedListNode head) {
          /*
         SinglyLinkedListNode {
         int data;
         SinglyLinkedListNode next;
     }
     */
        Stack<Integer> stack = new Stack<>();
        SinglyLinkedListNode iter = head;
        while (iter != null) {
            stack.add(iter.data);
            iter = iter.next;
        }
        iter = head;
        while (!stack.isEmpty()) {
            iter.data = stack.pop();
            iter = iter.next;
        }
        return head;
    }
}
