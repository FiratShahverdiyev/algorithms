package com.company.linkedlist;

import java.util.HashSet;
import java.util.Set;

public class FindMergePoint {
    /*
     * For your reference:
     *
     * SinglyLinkedListNode {
     *     int data;
     *     SinglyLinkedListNode next;
     * }
     *
     */
    static int findMergeNode(SinglyLinkedListNode head1, SinglyLinkedListNode head2) {
        SinglyLinkedListNode iter1 = head1;
        SinglyLinkedListNode iter2 = head2;
        Set<SinglyLinkedListNode> hashSet = new HashSet<>();
        while (iter1 != null) {
            hashSet.add(iter1);
            iter1 = iter1.next;
        }
        while (!hashSet.contains(iter2)) {
            iter2 = iter2.next;
        }
        return iter2.data;
    }
}
