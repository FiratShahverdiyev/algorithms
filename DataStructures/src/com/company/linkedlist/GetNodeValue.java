package com.company.linkedlist;

public class GetNodeValue {
    /*
     * For your reference:
     *
     * SinglyLinkedListNode {
     *     int data;
     *     SinglyLinkedListNode next;
     * }
     *
     */
    static int getNode(SinglyLinkedListNode head, int positionFromTail) {
        if (head == null)
            return 0;
        SinglyLinkedListNode iter = head;
        int length = 0;
        while (iter != null) {
            iter = iter.next;
            length++;
        }
        int position = length - positionFromTail;
        iter = head;
        for (int i = 0; i < position - 1; i++) {
            iter = iter.next;
        }
        return iter.data;
    }
}
