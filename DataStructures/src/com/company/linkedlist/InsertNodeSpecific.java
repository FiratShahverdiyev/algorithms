package com.company.linkedlist;

public class InsertNodeSpecific {
    /*
         SinglyLinkedListNode {
         int data;
         SinglyLinkedListNode next;
     }
     */
    static SinglyLinkedListNode insertNodeAtPosition(SinglyLinkedListNode head, int data, int position) {
        if (head == null)
            return new SinglyLinkedListNode(data);

        SinglyLinkedListNode iter = head;
        if (position == 0) {
            SinglyLinkedListNode newHead = new SinglyLinkedListNode(data);
            newHead.next = head;
            return newHead;
        }
        for (int i = 0; i < position - 1; i++) {
            iter = iter.next;
        }
        if (iter.next == null) {
            iter.next = new SinglyLinkedListNode(data);
        } else {
            SinglyLinkedListNode temp = iter.next;
            iter.next = new SinglyLinkedListNode(data);
            iter.next.next = temp;
        }
        return head;
    }
}
