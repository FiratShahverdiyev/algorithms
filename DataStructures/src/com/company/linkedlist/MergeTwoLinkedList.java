package com.company.linkedlist;

public class MergeTwoLinkedList {
    /*
         SinglyLinkedListNode {
         int data;
         SinglyLinkedListNode next;
     }
     */
    static SinglyLinkedListNode mergeLists(SinglyLinkedListNode head1, SinglyLinkedListNode head2) {
        SinglyLinkedListNode iter1 = head1;
        SinglyLinkedListNode iter2 = head2;
        SinglyLinkedListNode newHead = null;
        SinglyLinkedListNode iter = null;
        boolean flag = true;
        while (iter1 != null && iter2 != null) {
            if (iter1.data <= iter2.data) {
                if (flag) {
                    newHead = iter1;
                    iter = newHead;
                    flag = false;
                } else {
                    iter.next = iter1;
                    iter = iter.next;
                }
                iter1 = iter1.next;
            } else {
                if (flag) {
                    newHead = iter2;
                    iter = newHead;
                    flag = false;
                } else {
                    iter.next = iter2;
                    iter = iter.next;
                }
                iter2 = iter2.next;
            }
        }
        while (iter2 != null) {
            if (flag) {
                newHead = iter2;
                iter = newHead;
                flag = false;
            } else {
                iter.next = iter2;
                iter = iter.next;
            }
            iter2 = iter2.next;
        }
        while (iter1 != null) {
            if (flag) {
                newHead = iter1;
                iter = newHead;
                flag = false;
            } else {
                iter.next = iter1;
                iter = iter.next;
            }
            iter1 = iter1.next;
        }
        return newHead;
    }
}
