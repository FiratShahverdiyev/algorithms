package com.company.linkedlist;

public class InsertNodeAtHead {
    /*
         SinglyLinkedListNode {
         int data;
         SinglyLinkedListNode next;
     }
     */
    static SinglyLinkedListNode insertNodeAtHead(SinglyLinkedListNode llist, int data) {
        if (llist == null) {
            return new SinglyLinkedListNode(data);
        }
        SinglyLinkedListNode newHead = new SinglyLinkedListNode(data);
        newHead.next = llist;
        llist = newHead;
        return llist;
    }
}
