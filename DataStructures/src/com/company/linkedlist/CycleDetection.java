package com.company.linkedlist;

import java.util.HashSet;
import java.util.Set;

public class CycleDetection {
    /*
     * For your reference:
     *
     * SinglyLinkedListNode {
     *     int data;
     *     SinglyLinkedListNode next;
     * }
     *
     */
    static boolean hasCycle(SinglyLinkedListNode head) {
        Set<SinglyLinkedListNode> hashSet = new HashSet<>();
        SinglyLinkedListNode iter = head;
        while (iter != null) {
            if (hashSet.contains(iter)) {
                return true;
            }
            hashSet.add(iter);
            iter = iter.next;
        }
        return false;
    }
}
