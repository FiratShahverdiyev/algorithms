package com.company.linkedlist;

public class InsertingNodeSortedDoubly {
    /*
     * For your reference:
     *
     * DoublyLinkedListNode {
     *     int data;
     *     DoublyLinkedListNode next;
     *     DoublyLinkedListNode prev;
     * }
     *
     */
    static DoublyLinkedListNode sortedInsert(DoublyLinkedListNode head, int data) {
        if (head == null) {
            head = new DoublyLinkedListNode(data);
            return head;
        }
        if (head.data >= data) {
            head.prev = new DoublyLinkedListNode(data);
            head.prev.next = head;
            head = head.prev;
            return head;
        }
        DoublyLinkedListNode iter = head;
        while (true) {
            if (data <= iter.data) {
                DoublyLinkedListNode temp;
                iter = iter.prev;
                temp = iter.next;
                iter.next = new DoublyLinkedListNode(data);
                iter.next.next = temp;
                return head;
            } else {
                if (iter.next == null) {
                    break;
                }
                iter = iter.next;
            }
        }
        iter.next = new DoublyLinkedListNode(data);
        return head;
    }
}
