package com.company.linkedlist;

public class DeleteNode {
    /*
         SinglyLinkedListNode {
         int data;
         SinglyLinkedListNode next;
     }
     */
    static SinglyLinkedListNode deleteNode(SinglyLinkedListNode head, int position) {
        if (position == 0)
            return head.next;

            SinglyLinkedListNode tail = head;
            for (int i = 0; i < position - 1; i++) {
                tail = tail.next;
            }
            if (tail.next.next != null)
                tail.next = tail.next.next;
            else {
                tail.next = null;
            }
        return head;
    }
}
