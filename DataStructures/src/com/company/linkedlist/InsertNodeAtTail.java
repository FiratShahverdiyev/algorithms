package com.company.linkedlist;

public class InsertNodeAtTail {
    /*
         SinglyLinkedListNode {
         int data;
         SinglyLinkedListNode next;
     }
     */
    static SinglyLinkedListNode insertNodeAtTail(SinglyLinkedListNode head, int data) {
        if (head == null) {
            return new SinglyLinkedListNode(data);
        }

        SinglyLinkedListNode tail = head;
        while (tail.next != null) {
            tail = tail.next;
        }
        tail.next = new SinglyLinkedListNode(data);
        return head;
    }
}
