package com.company.linkedlist;

public class ReverseDoublyLinkedList {
    /*
     * For your reference:
     *
     * DoublyLinkedListNode {
     *     int data;
     *     DoublyLinkedListNode next;
     *     DoublyLinkedListNode prev;
     * }
     *
     */
    static DoublyLinkedListNode reverse(DoublyLinkedListNode head) {
        DoublyLinkedListNode iter = head;
        DoublyLinkedListNode head2 = new DoublyLinkedListNode(0);
        DoublyLinkedListNode iter2 = head2;
        while (iter.next != null) {   //1 2 3 4
            iter = iter.next;
        }
        while (iter != head) {
            iter2.data = iter.data;
            iter2.next = new DoublyLinkedListNode(0);
            iter2 = iter2.next;
            iter = iter.prev;
        }
        iter2.data = iter.data;
        return head2;
    }
}
