package com.company.intermadiate;

import java.util.ArrayList;
import java.util.List;

public class UserFriendlyPasswordSystem {

    static String password = "!";
    static long sum = 0l;
    static long diff_sum = 0l;

    public static List<Integer> authEvents(List<List<String>> events) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < events.size(); i++) {
            String order = events.get(i).get(0);
            String code = events.get(i).get(1);
            if (order.equals("setPassword")) {
                hashFunction(code);
            } else {
                long authorize = Long.parseLong(code);
                //  System.out.println("auth"+authorize);
                //  System.out.println("diff_sum"+ diff_sum);
                long diff = authorize - diff_sum;
                //   System.out.println(diff);
                if (code.equals(password) || (diff <= 122 && diff >= 48))
                    result.add(1);
                else
                    result.add(0);
            }
        }
        return result;
    }

     static int getAsciiValue(Character c) {
        return (int) c;
    }

     static long getResult(int ascii, int pow) {
        return (long)(Math.pow(131, pow) * ascii);
    }
    static void hashFunction(String x){
        sum = 0l;
        diff_sum = 0l;
        for (int j = 0; j < x.length(); j++) {
            sum += getResult(getAsciiValue(x.charAt(j)), x.length() - 1 - j);
            diff_sum += getResult(getAsciiValue(x.charAt(j)), x.length() - j);
        }
        sum = sum % (long) (Math.pow(10, 9) + 7);
        diff_sum = diff_sum % (long) (Math.pow(10, 9) + 7);
        password = String.valueOf(sum);
    }
    static void authenticate(String hashCode){

    }

}
