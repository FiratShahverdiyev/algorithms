package com.company.tree;

/*
    class Node
    	int data;
    	Node left;
    	Node right;
	*/
public class LowestCommonAncestor {
    public static Node lca(Node root, int v1, int v2) {
        Node iter = root;
        while (iter != null) {
            if ((v1 >= iter.data && v2 <= iter.data) || (v1 <= iter.data && v2 >= iter.data)) {
                return iter;
            } else if (v1 >= iter.data && v2 >= iter.data) {
                iter = iter.right;
            } else if (v1 <= iter.data && v2 <= iter.data) {
                iter = iter.left;
            } else {
                return iter;
            }
        }
        return null;
    }
}
