package com.company.tree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/*
   class Node
       int data;
       Node left;
       Node right;
   */
public class TopView {
    public static void topView(Node root) {
        if (root == null)
            return;
        HashMap<Integer, Node> map = new HashMap<>();
        HashMap<Node, Integer> nodeMap = new HashMap<>();
        map.put(0, root);
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        nodeMap.put(root, 0);
        Node iter;
        // System.out.print(root.data+" ");
        while (!queue.isEmpty()) {
            iter = queue.poll();
            if (!map.containsKey(nodeMap.get(iter))) {
                map.put(nodeMap.get(iter), iter);
                // System.out.print(iter.data+" ");
            }
            if (iter.left != null) {
                queue.add(iter.left);
                nodeMap.put(iter.left, nodeMap.get(iter) - 1);
            }
            if (iter.right != null) {
                queue.add(iter.right);
                nodeMap.put(iter.right, nodeMap.get(iter) + 1);
            }
        }
        List<Integer> list = new ArrayList<>(map.keySet());
        Collections.sort(list);
        for (int i = 0; i < list.size(); i++) {
            System.out.print(map.get(list.get(i)).data + " ");
        }

    }
}
