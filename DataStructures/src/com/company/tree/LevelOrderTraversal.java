package com.company.tree;

import java.util.LinkedList;
import java.util.Queue;

/*
    class Node
    	int data;
    	Node left;
    	Node right;
*/
public class LevelOrderTraversal {
    public static void levelOrder(Node root) {
        if (root == null)
            return;
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            root = queue.poll();
            System.out.print(root.data + " ");
            if (root.left != null) {
                queue.add(root.left);
            }
            if (root.right != null) {
                queue.add(root.right);
            }
        }
    }
}
