package com.company.tree;

/*
   class Node
       int data;
       Node left;
       Node right;
   */
public class HeightOfBinaryTree {
    public static int height(Node root) {
        if (root == null)
            return -1;
        int h;
        int left = height(root.left);
        int right = height(root.right);
        if (left > right) {
            h = 1 + left;
        } else {
            h = 1 + right;
        }
        return h;
    }
}
