package com.company.tree;

/*
	class Node
		public  int frequency; // the frequency of this tree
    	public  char data;
    	public  Node left, right;

*/
public class HuffmanDecoding {
    void decode(String s, Node root) {
        Node iter = root;
        int i = 0;
        while (i < s.length()) {
            if (s.charAt(i) == '0') {
                iter = iter.left;
                if (Character.isAlphabetic(iter.data) || iter.data == '?' || iter.data == '!') {
                    System.out.print(iter.data);
                    iter = root;
                    i++;
                    continue;
                } else {
                    i++;
                }
            } else {
                iter = iter.right;
                if (Character.isAlphabetic(iter.data) || iter.data == '?' || iter.data == '!') {
                    System.out.print(iter.data);
                    iter = root;
                    i++;
                    continue;
                } else {
                    i++;
                }
            }
        }
    }
}
