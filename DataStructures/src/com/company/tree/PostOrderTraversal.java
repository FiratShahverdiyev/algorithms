package com.company.tree;

/*
class Node {
    int data;
    Node left;
    Node right;
}
*/
public class PostOrderTraversal {
    public static void postOrder(Node root) {
        if (root == null)
            return;
        postOrder(root.left);
        postOrder(root.right);
        System.out.print(root.data + " ");
    }
}
