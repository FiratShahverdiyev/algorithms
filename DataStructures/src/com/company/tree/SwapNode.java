package com.company.tree;

public class SwapNode {
    public static int[] left;
    public static int[] right;
    public static int[] depth;
    static int[][] result;
    static int index2 = 0;

    static void calc_depth(int cur, int d) {
        depth[cur] = d;
        if (left[cur] > 0) {
            calc_depth(left[cur], d + 1);
        }
        if (right[cur] > 0) {
            calc_depth(right[cur], d + 1);
        }
    }

    static void inorder(int node, int index) {
        if (left[node] > 0)
            inorder(left[node], index);

        result[index][index2++] = node;

        if (right[node] > 0)
            inorder(right[node], index);
    }

    static int[][] swapNodes(int[][] indexes, int[] queries, int n) {
        result = new int[queries.length][n];
        left = new int[1025];
        right = new int[1025];
        depth = new int[1025];
        for (int i = 0; i < indexes.length; i++) {
            left[i + 1] = indexes[i][0];
            right[i + 1] = indexes[i][1];
        }
        calc_depth(1, 1);
        for (int k = 0; k < queries.length; k++) {
            for (int i = 1; i < depth.length; i++) {
                if (depth[i] % queries[k] == 0) {
                    int temp = left[i];
                    left[i] = right[i];
                    right[i] = temp;
                }
            }
            index2 = 0;
            inorder(1, k);
        }
        return result;
    }
}
