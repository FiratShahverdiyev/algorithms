package com.company.tree;

import java.util.ArrayList;
import java.util.List;
/*
class Node {
    int data;
    Node left;
    Node right;
}
 */

public class IsThisBinarySearchTree {
    static List<Node> list = new ArrayList<>();

    boolean checkBST(Node root) {
        inOrder(root);
        return checkAscending();
    }

    void inOrder(Node root) {
        if (root == null) {
            return;
        }
        inOrder(root.left);
        list.add(root);
        inOrder(root.right);
    }

    boolean checkAscending() {
        for (int i = 0; i < list.size() - 1; i++) {
            if (list.get(i).data >= list.get(i + 1).data)
                return false;
        }
        return true;
    }
}
