package com.company.tree;

import java.util.LinkedList;
import java.util.Queue;

public class BinarySearchTreeInsertion {
    public static Node insert(Node root, int data) {
        if (root == null)
            return new Node(data);
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            Node iter = queue.poll();
            if (data >= iter.data && iter.right != null)
                queue.add(iter.right);
            else if (data >= iter.data) {
                iter.right = new Node(data);
                return root;
            }
            if (data <= iter.data && iter.left != null)
                queue.add(iter.left);
            else if (data <= iter.data) {
                iter.left = new Node(data);
                return root;
            }
        }
        return root;
    }
}
