package com.company.heap;

import java.util.PriorityQueue;

public class JessieAndCookies {
    static int cookies(int k, int[] A) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        for (int i = 0; i < A.length; i++) {
            pq.add(A[i]);
        }
        int count = 0;
        while (pq.size() >= 2) {
            if (pq.peek() < k) {
                pq.add((pq.poll() + pq.poll() * 2));
                count++;
            } else {
                break;
            }
        }
        if (pq.peek() >= k) {
            return count;
        }
        return -1;
    }
}
